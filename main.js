// ./main.js
// de electron nos traemos app y BrowserWindow
const {app, Menu, BrowserWindow} = require('electron');
var url= require('url');
var path= require('path');
var ipcMain = require('electron').ipcMain;
var request = require('request');
var config  = {
  consumerkey: 'hOwryZxjZlur9akxMlfRQpOrJ',
  consumersecret: '8qDLreIruNwr6BlJOge2gGRUeGpff6nMay6DY2g6C8C2Qrck9f',
  bearertoken: ''
};

var fs = require('fs');



ipcMain.on('usuario', function(event) {
  var header = config.consumerkey + ':' +config.consumersecret;
  var encheader = new Buffer(header).toString('base64');
  var finalheader = 'Basic ' + encheader;
  
  request.post('https://api.twitter.com/oauth2/token', {form: {'grant_type': 'client_credentials'}, 
    headers: {Authorization: finalheader}}, function(error, response, body) {
        if(error)
            console.log(error);
        else {
            config.bearertoken = JSON.parse(body).access_token;
            console.log(config.bearertoken);
            event.sender.send('usuario_reply',config.bearertoken);
        }
        
    });
  
  
});



ipcMain.on('perfil', function(event, arg) {
  var searchquery = arg;
  var encsearchquery = encodeURIComponent(searchquery);
  var bearerheader = 'Bearer ' + config.bearertoken;
  request.get('https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=' + encsearchquery + '&count=2', {headers: {Authorization: bearerheader}}, function(error, body, response) {
        if(error)
          console.log(error);
        else {
          event.sender.send('perfil_reply',body.body);
        }
  });
});

ipcMain.on('buscame', function(event, arg) {
  var searchquery = arg;
  var encsearchquery = encodeURIComponent(searchquery);
  var bearerheader = 'Bearer ' + config.bearertoken;
  request.get('https://api.twitter.com/1.1/search/tweets.json?q=' + encsearchquery +
  '&result_type=recent'+ '&count=2', {headers: {Authorization: bearerheader}}, function(error, body, response) {
      if(error)
      console.log(error);
      else {
        event.sender.send('buscame_reply',body.body);
      }
  });
});
ipcMain.on('leeme', function(event) {
  var ubic=process.env.APPDATA+'/misdatos.json';
  fs.readFile(ubic, 'utf-8', function (err, data) {
    if(err){
        alert("Ha ocurrido un error abriendo el archivo:" + err.message);
        return;
    }
    // Cambia cómo manipular el contenido del archivo
    //console.log("El contenido del archivo es : " + data);
    event.sender.send('leeme_reply',data);
  });
});
ipcMain.on('guardame', function(event, arg) {
  var ubic=process.env.APPDATA+'/misdatos.json';
  
  //Replace the file with a new one:
  fs.writeFile(ubic, arg,  (err)=> {
    if (err) {
      console.log('Error');
      return;
    }
    console.log('Replaced!');
  });
  
});


// definimos win, donde se va a guardar la instancia de BrowserWindow actual
let win = null;
// definimos la función createWindow
function createWindow() {
  // instanciamos BrowserWindow, esto va a iniciar un proceso renderer
  win = new BrowserWindow({'width': 1000, 
                           'height': 700,
                           'minWidth':1000,
                           'minHeight': 700,
                           'frame':false,
                           'icon':path.join(__dirname, './src/favicon.ico'),});
  // Specify entry point
  //win.loadURL('http://localhost:4200');
  
  win.loadURL(url.format({
    pathname:path.join(__dirname, './build/index.html'),
    protocol:'file:',
    slashes:true
  }));

  
  // Show dev tools
  // Remove this line before distributing
  //win.webContents.openDevTools();



  // Remove window once app is closed
  // escuchamos el evento `closed` de la ventana para limpar la variable `window`
  // de esta forma permitimos matar la ventana sin matar la aplicación
  win.on('closed', function () {  
    win = null;
  });

  win.once('ready-to-show', () =>{
    setTimeout(win.show(),3000);
    
  });
  
}
// cuando nuestra app haya terminado de iniciar va a disparar el evento `ready`
// lo escuchamos y ejecutamos la función createWindow
app.on('ready', function () {
  createWindow();
  Menu.setApplicationMenu(null);
});
app.on('activate', () => {
  if (win === null) {
    createWindow()
  }
})
// escuchamos el evento window-all-closed y si no estamos en Mac cerramos la aplicación
// lo de Mac es debido a que en este SO es común que se pueda cerrar todas las ventanas sin cerrar
// la aplicación completa
app.on('window-all-closed', function () {
  if (process.platform != 'darwin') {
    app.quit();
  }
   
});