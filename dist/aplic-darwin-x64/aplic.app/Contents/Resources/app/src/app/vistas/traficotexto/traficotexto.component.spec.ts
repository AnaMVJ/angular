import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TraficotextoComponent } from './traficotexto.component';

describe('TraficotextoComponent', () => {
  let component: TraficotextoComponent;
  let fixture: ComponentFixture<TraficotextoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TraficotextoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TraficotextoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
