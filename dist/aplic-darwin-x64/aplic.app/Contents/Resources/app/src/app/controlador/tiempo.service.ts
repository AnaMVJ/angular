import { Injectable } from '@angular/core';
import { HttpClient , HttpHeaders} from '@angular/common/http';
import {Http, Headers} from '@angular/http';
import {TIEMPO} from '../controlador/controlconfig';
import {Forecast} from '../modelos/forecast';
  export interface Main {
        temp: number;
        temp_min: number;
        temp_max: number;
        pressure: number;
        sea_level: number;
        grnd_level: number;
        humidity: number;
        temp_kf: number;
    }

    export interface Weather {
        id: number;
        main: string;
        description: string;
        icon: string;
    }

    export interface Clouds {
        all: number;
    }

    export interface Wind {
        speed: number;
        deg: number;
    }

    export interface Rain {
    }

    export interface Sys {
        pod: string;
    }

    export interface List {
        dt: number;
        main: Main;
        weather: Weather[];
        clouds: Clouds;
        wind: Wind;
        rain: Rain;
        sys: Sys;
        dt_txt: string;
    }

    export interface Coord {
        lat: number;
        lon: number;
    }

    export interface City {
        id: number;
        name: string;
        coord: Coord;
        country: string;
    }

    export interface RootObject {
        cod: string;
        message: number;
        cnt: number;
        list: List[];
        city: City;
    }

@Injectable()
export class TiempoService {
  tiemp=TIEMPO;
  constructor(private http:HttpClient) { }
  tiempo(){
          
          return this.http.get<RootObject>('https://api.openweathermap.org/data/2.5/forecast?lat='+this.tiemp.Lat+'&lon='+this.tiemp.Lon+'&appid=12217a05e7ae0e4f83ef27620ef75943&lang=es');   
  }
  

}
