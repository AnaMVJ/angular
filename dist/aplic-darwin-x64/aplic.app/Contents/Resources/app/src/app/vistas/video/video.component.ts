import { Component, OnInit } from '@angular/core';
import { VIDEO } from '../../controlador/controlconfig';
import {DomSanitizer} from '@angular/platform-browser';
import { SafeUrl } from '@angular/platform-browser/src/security/dom_sanitization_service';
@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.css']
})
export class VideoComponent implements OnInit {
  video=VIDEO;
  videoNode;
  constructor(private sanitizer:DomSanitizer) { }

  ngOnInit() {
    this.videoNode = document.querySelector('#mivideo');
    if(this.video.visible){
      if(this.video.localoweb){
        console.log(this.video.file);
        this.videoNode.src = window.URL.createObjectURL(this.video.file);
      } else{
        this.videoNode.src = new window.URL(this.video.lugar);
      }
    }
  }
  

}
