import { Component, OnInit } from '@angular/core';
import { TRAFICO } from '../../controlador/controlconfig';
import { ViewChild } from '@angular/core';
import { } from '@types/googlemaps';
import { TraficoService } from '../../controlador/trafico.service';
import { Observable, Observer } from 'rxjs';
declare var google: any;
@Component({
  selector: 'app-trafico',
  templateUrl: './trafico.component.html',
  styleUrls: ['./trafico.component.css']
})
export class TraficoComponent implements OnInit {
  @ViewChild('gmap') gmapElement: any;
  map: google.maps.Map;
  trafico = TRAFICO;
  constructor() { }

  ngOnInit() {
    var coords;
    var mapProp = {
      center: new google.maps.LatLng(this.trafico.Lat, this.trafico.Lon),
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    this.map = new google.maps.Map(this.gmapElement.nativeElement, mapProp);
    var trafficLayer = new google.maps.TrafficLayer();
    trafficLayer.setMap(this.map);
        
  }
  
}
  



