import { Component, OnInit } from '@angular/core';
import { ViewChild } from '@angular/core';
import { } from '@types/googlemaps';
import { TIEMPO } from '../../controlador/controlconfig';
import { Observable, Observer } from 'rxjs';
import { WeathermapService } from '../../controlador/weathermap.service';
@Component({
  selector: 'app-tiempomapa',
  templateUrl: './tiempomapa.component.html',
  styleUrls: ['./tiempomapa.component.css']
})
export class TiempomapaComponent implements OnInit {
  @ViewChild('gmap') gmapElement: any;
  map: google.maps.Map;
  tiempo=TIEMPO;
  geoJSON= {
    type: "FeatureCollection",
    features: []
  };
  infowindow;
  constructor(public _tiempomapaservice:WeathermapService) { }

  ngOnInit() {
    var coords;
    var mapProp = {
      center: new google.maps.LatLng(this.tiempo.Lat, this.tiempo.Lon),
      zoom: 10,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    this.map = new google.maps.Map(this.gmapElement.nativeElement, mapProp);
    google.maps.event.addListener(this.map,'tilesloaded', () => {
        var bounds = this.map.getBounds();
        console.log(bounds);
        var NE = bounds.getNorthEast();
        var SW = bounds.getSouthWest();
        this.getWeather(NE.lat(), NE.lng(), SW.lat(), SW.lng());
        // Sets up and populates the info window with details
        this.map.data.addListener('click', function(event) {
            this.infowindow=new google.maps.InfoWindow({
              content:"<img src=" + event.feature.getProperty("icon") + ">"
                      + "<br /><strong>" + event.feature.getProperty("city") + "</strong>"
                      + "<br />" + event.feature.getProperty("temperature") + "&deg;C"
                      + "<br />" + event.feature.getProperty("weather")
            });
            this.infowindow.setOptions({
                position:{
                  lat: event.latLng.lat(),
                  lng: event.latLng.lng()
                },
                pixelOffset: {
                  width: 0,
                  height: -15
                }
            });
            this.infowindow.open(this.map);
        });
    });
  }
  getWeather(northLat, eastLng, southLat,westLng) {
    this._tiempomapaservice.tiempo(westLng,northLat, eastLng, southLat).subscribe(data=>{
      console.log(data);
      if(data.list.length> 0) {
        for (var i = 0; i < data.list.length; i++) {
          this.geoJSON.features.push(this.jsonToGeoJson(data.list[i]));
        }
        this.drawIcons(this.geoJSON);
      }
    });
  }
  
   // For each result that comes back, convert the data to geoJSON
   jsonToGeoJson(weatherItem) {
    var feature = {
      type: "Feature",
      properties: {
        city: weatherItem.name,
        weather: weatherItem.weather[0].description,
        temperature: weatherItem.main.temp,
        min: weatherItem.main.temp_min,
        max: weatherItem.main.temp_max,
        humidity: weatherItem.main.humidity,
        pressure: weatherItem.main.pressure,
        icon: "http://openweathermap.org/img/w/"
              + weatherItem.weather[0].icon  + ".png",
        coordinates: [weatherItem.coord.Lon, weatherItem.coord.Lat]
      },
      geometry: {
        type: "Point",
        coordinates: [weatherItem.coord.Lon, weatherItem.coord.Lat]
      }
    };
     // Set the custom marker icon
     this.map.data.setStyle(function(feature) {
      return {
        icon: {
          url: feature.getProperty('icon'),
          anchor: new google.maps.Point(25, 25)
        }
      };
    });
    // returns object
    return feature;
  }
  // Add the markers to the map
  drawIcons (weather) {
    this.map.data.addGeoJson(this.geoJSON);
  }
}
