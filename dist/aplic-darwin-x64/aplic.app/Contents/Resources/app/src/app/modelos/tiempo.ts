export class Tiempo{
    visible:boolean;
    lugar:String;
    icono:String;
    Lon:number;
    Lat:number;
    mapa:boolean
    constructor( tiemp:Tiempo) 
    {     
        this.visible=tiemp.visible;
        this.lugar=tiemp.lugar;
        this.icono=tiemp.icono;
        this.Lon=tiemp.Lon;
        this.Lat=tiemp.Lat;
        this.mapa=tiemp.mapa;
    }
    
    
}