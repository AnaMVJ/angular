export class Forecast{
    public description:string;
    public temp:number;
    public tempmax:number;
    public tempmin:number;
    public date:string;
    public img:string;
    public humidity:string;
    constructor(  description,temp,tempmax,tempmin,date,img,humidity) 
    {  
        this.description=description;
        this.temp=parseFloat(temp)-273;
        this.tempmax=parseFloat(tempmax)-273;
        this.tempmin=parseFloat(tempmin)-273;
        this.date=date;
        this.img=img;   
        this.humidity=humidity;
    }

}