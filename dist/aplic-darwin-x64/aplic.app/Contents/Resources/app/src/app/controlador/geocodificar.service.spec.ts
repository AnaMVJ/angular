import { TestBed, inject } from '@angular/core/testing';

import { GeocodificarService } from './geocodificar.service';

describe('GeocodificarService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GeocodificarService]
    });
  });

  it('should be created', inject([GeocodificarService], (service: GeocodificarService) => {
    expect(service).toBeTruthy();
  }));
});
