import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Videos } from '../../modelos/videos';
import { Twitter } from '../../modelos/twitter';
import { FormsModule } from '@angular/forms'; // <-- NgModel lives here
import { TWITS, ControlConfig, VIDEO2, TRAFICO2, TITULO2, TIEMPO2, TWITS2 } from '../../controlador/controlconfig';
import { VIDEO } from '../../controlador/controlconfig';
import { TIEMPO } from '../../controlador/controlconfig';
import { TRAFICO } from '../../controlador/controlconfig';
import { TITULO } from '../../controlador/controlconfig';
import { CARGA } from '../../controlador/controlconfig';
import { TraficoService } from '../../controlador/trafico.service';
import { MessageServiceService } from '../../controlador/message-service.service';
import { GeocodificarService } from '../../controlador/geocodificar.service';
import { PreferenciaService } from '../../controlador/preferencia.service';
import {Router} from '@angular/router';
import { Tiempo } from '../../modelos/tiempo';
import { Trafico } from '../../modelos/trafico';
@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.css']
})
export class ConfigurationComponent implements OnInit {
  videos = VIDEO2;
  twitss = TWITS2;
  tiempos = TIEMPO2;
  traficos = TRAFICO2;
  titulos = TITULO2.titulo;
  video = VIDEO;
  twits = TWITS;
  tiempo = TIEMPO;
  trafico = TRAFICO;
  titulo = TITULO;
  carga=CARGA;
  can=false;
  file="";
  cambiopanel=false;
  controlconfig: ControlConfig;
  data;
  constructor(private router: Router, private route: ActivatedRoute,private geocod:GeocodificarService,private prefserv:PreferenciaService,
    private location: Location, private _traficoservice: TraficoService, private messageService: MessageServiceService) {
    this.controlconfig = new ControlConfig();

  }
  cancelame(){
    this.controlconfig.copiavideos(this.videos);
    this.controlconfig.copiatwits(this.twitss);
    this.controlconfig.copiatiempo(this.tiempos);
    this.controlconfig.copiatraf(this.traficos);
    this.titulo.titulo=this.titulos;
    this.redirigeme();
    
    
  }
  redirigeme(){
    this.router.navigate(['/vista']);
  }
  acceptar(){
    /*
    this.controlconfig.copiavideos2(this.video);
    this.controlconfig.copiatwits2(this.twits);
    this.controlconfig.copiatiempo2(this.tiempo);
    this.controlconfig.copiatraf2(this.trafico);
    */
    this.sendMessage(this.titulo.titulo);
    this.router.navigate(['/vista']);
  }

  loadData(){
    
    this.prefserv.preferencia();
    this.carga.load=false;
    this.sendMessage(this.titulo.titulo);
    
  }
  
  ngOnInit() {
    this.loadData();
    
    
  }

  goBack(): void {
    this.location.back();
  }
  onFileChange(event) {
    var url;
    this.file = event.target.files[0];
    this.controlconfig.onFileChange(this.file);
    console.log(this.file);


  }
  cambio(){
    this.cambiopanel=true;
  }
  onPlaceChange(event) {
    this.geocod.geocodif();
  }
  onPlaceChangetiempo(event) {
    this.geocod.geocodiftiempo();
  }
  sendMessage(mimensaje:string): void {
    // send message to subscribers via observable subject
    this.messageService.sendMessage(mimensaje);
  }

  clearMessage(): void {
    // clear message
    this.messageService.clearMessage();
  }

}
