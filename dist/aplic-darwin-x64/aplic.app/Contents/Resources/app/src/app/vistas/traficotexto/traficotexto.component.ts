import { Component, OnInit } from '@angular/core';
import { TraficoService } from '../../controlador/trafico.service';
import { TRAFICO } from '../../controlador/controlconfig';

@Component({
  selector: 'app-traficotexto',
  templateUrl: './traficotexto.component.html',
  styleUrls: ['./traficotexto.component.css']
})
export class TraficotextoComponent implements OnInit {
  trafico=TRAFICO;
  datos;
  constructor(private _trafServ: TraficoService) { }

  ngOnInit() {
    this._trafServ.trafico().subscribe(data => {

      this.datos = data.resourceSets[0].resources;
      console.log(this.datos);

    });;
  }

}
