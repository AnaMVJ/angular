import { Component } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { ConfigurationComponent } from './vistas/configuration/configuration.component';
import { VistaComponent } from './vistas/vista/vista.component';
import { Subscription } from 'rxjs/Subscription';

import { MessageServiceService } from './controlador/message-service.service';
import {ElectronService} from 'ngx-electron';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Panel';
  message: any;
  subscription;
  window;

  constructor(private _electronService: ElectronService, private messageService: MessageServiceService) {
    // subscribe to messages
    this.subscription = this.messageService.getMessage().subscribe(message => { console.log(message); this.title = message.text; });
    this.window=this._electronService.remote.getCurrentWindow();
    this._electronService.ipcRenderer.send('usuario');
  }

  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.subscription.unsubscribe();
  }
  maximize(){
    if (!this.window.isMaximized()) {
      this.window.maximize();
    } else {
      this.window.unmaximize();
    }
  }
  minimize(){
      this.window.minimize();
  }
  close(){
      this.window.close();
  }
}
