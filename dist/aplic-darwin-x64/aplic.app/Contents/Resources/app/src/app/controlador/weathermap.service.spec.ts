import { TestBed, inject } from '@angular/core/testing';

import { WeathermapService } from './weathermap.service';

describe('WeathermapService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WeathermapService]
    });
  });

  it('should be created', inject([WeathermapService], (service: WeathermapService) => {
    expect(service).toBeTruthy();
  }));
});
