import { TestBed, inject } from '@angular/core/testing';

import { TiempoService } from './tiempo.service';

describe('TiempoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TiempoService]
    });
  });

  it('should be created', inject([TiempoService], (service: TiempoService) => {
    expect(service).toBeTruthy();
  }));
});
