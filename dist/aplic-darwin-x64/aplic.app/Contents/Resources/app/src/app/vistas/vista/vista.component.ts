import { Component, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { TWITS } from '../../controlador/controlconfig';
import { VIDEO } from '../../controlador/controlconfig';
import { TIEMPO } from '../../controlador/controlconfig';
import { TITULO } from '../../controlador/controlconfig';
import {TRAFICO} from '../../controlador/controlconfig';
import {DomSanitizer} from '@angular/platform-browser';
import { SafeUrl } from '@angular/platform-browser/src/security/dom_sanitization_service';
import { MistwitsService } from '../../controlador/mistwits.service';
import { NgIf } from '@angular/common';
import {VideoComponent} from '../video/video.component';
import {TwitterComponent} from '../twitter/twitter.component';
import { TiempoService } from '../../controlador/tiempo.service';
import { WeathermapService } from '../../controlador/weathermap.service';
import { TraficoService } from '../../controlador/trafico.service';
import {TiempoComponent} from "../tiempo/tiempo.component";
import {TraficoComponent} from "../trafico/trafico.component";
import {TraficotextoComponent} from "../traficotexto/traficotexto.component";
import {TiempomapaComponent}from "../tiempomapa/tiempomapa.component";
import { PreferenciaService } from '../../controlador/preferencia.service';

@Component({
  selector: 'app-vista',
  templateUrl: './vista.component.html',
  styleUrls: ['./vista.component.css']
})
export class VistaComponent implements OnInit {
  twits=TWITS;
  video=VIDEO;
  tiempo=TIEMPO;
  trafico=TRAFICO;
  titulo=TITULO;
  mytweets;
  twit;
  tam;
  window;
  tiempomapac:TiempomapaComponent;
  videoc:VideoComponent;
  tiempoc:TiempoComponent;
  traficotc:TraficotextoComponent;
  constructor( private route: ActivatedRoute,private prefserv:PreferenciaService,
    private location: Location,private sanitizer:DomSanitizer,private _tiemposervice:TiempoService, private _twitservice:MistwitsService, private tiempoServ:TiempoService,private traficoServ:TraficoService,private _tiempomapaservice:WeathermapService) 
    { 
      var t={lugar:this.video.lugar,visible:this.video.visible,localoweb:this.video.localoweb}
      console.log(t);
      console.log(this.video);
        //file:this.video.file.path}
      var datos={titulo:this.titulo.titulo,videos:t,trafico:this.trafico,tiempo:this.tiempo,twitter:this.twits};
      const r=JSON.stringify(datos);
      this.prefserv.guardapreferencia(r);
      //setTimeout(5);
      this.traficotc=new TraficotextoComponent(traficoServ);
      this.videoc=new VideoComponent(sanitizer);
      this.twit=new TwitterComponent(_twitservice);
      this.tiempoc=new TiempoComponent(_tiemposervice);
      this.tiempomapac=new TiempomapaComponent(this._tiempomapaservice);
      
     
    }

  ngOnInit() {
    
    //this.prefserv.guardapreferencia();
    
   
    
    //console.log(this.mytweets);
    //this._twitservice.traficoytiempo();
  }
  
}
