var winInstaller = require('electron-winstaller');
var gulp = require('gulp');
gulp.task('create-windows-installer', function (done) {
        resultPromise = winInstaller.createWindowsInstaller({
                appDirectory: './aplic-win32-x64',
                outputDirectory: './installers',
                authors: 'Ana Maria Valdeon',
                exe: 'aplic.exe'
        });

        resultPromise.then(() => console.log("It worked!"), (e) => console.log(e.message));
});
