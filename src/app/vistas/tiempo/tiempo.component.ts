import { Component, OnInit } from '@angular/core';
import { TiempoService } from '../../controlador/tiempo.service';
import {Main} from '../../controlador/tiempo.service';
import {RootObject} from '../../controlador/tiempo.service';
import {Wind} from '../../controlador/tiempo.service';
import {Clouds} from '../../controlador/tiempo.service';
import {Weather} from '../../controlador/tiempo.service';
import {Coord} from '../../controlador/tiempo.service';
import {Sys} from '../../controlador/tiempo.service';
import {TIEMPO} from '../../controlador/controlconfig';
import {Forecast} from '../../modelos/forecast';
@Component({
  selector: 'app-tiempo',
  templateUrl: './tiempo.component.html',
  styleUrls: ['./tiempo.component.css']
})
export class TiempoComponent implements OnInit {
  forecast:Forecast[]=[];
  constructor(public _tiemposervice:TiempoService) {
    
   }

  ngOnInit() {
    this._tiemposervice.tiempo().subscribe(data => {
      console.log(data);
       for(let i=0; i<data.list.length;i= i+8){
          var f = new Forecast(data.list[i].weather[0].description,
                                data.list[i].main.temp,
                                data.list[i].main.temp_max,
                                data.list[i].main.temp_min,
                                data.list[i].dt_txt,
                                data.list[i].weather[0].icon,
                                data.list[i].main.humidity);
          this.forecast.push(f);
       }
       console.log(this.forecast);
    });
    
  }

}
