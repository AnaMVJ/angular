import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TiempomapaComponent } from './tiempomapa.component';

describe('TiempomapaComponent', () => {
  let component: TiempomapaComponent;
  let fixture: ComponentFixture<TiempomapaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TiempomapaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TiempomapaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
