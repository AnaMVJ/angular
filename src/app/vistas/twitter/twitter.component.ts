import { Component, NgZone, OnInit, OnDestroy } from '@angular/core';
import { MistwitsService } from '../../controlador/mistwits.service';
import { TWITS } from '../../controlador/controlconfig';
import { Status } from '../../controlador/mistwits.service';
import { Data } from '../../controlador/mistwits.service';
import { User} from '../../controlador/mistwits.service';
import { NgIf } from '@angular/common';


@Component({
  selector: 'app-twitter',
  templateUrl: './twitter.component.html',
  styleUrls: ['./twitter.component.css']
})
export class TwitterComponent implements OnInit {
  twits=TWITS;
  mytweets; 

  constructor( public _twitservice:MistwitsService) { }

  ngOnInit() {
    
    this._twitservice.call();
    
   
      console.log(this._twitservice.tweetsdata);
  
  }


}
