import { Injectable,NgZone } from '@angular/core';
import { HttpClient , HttpHeaders} from '@angular/common/http';
import {ElectronService} from 'ngx-electron';
import{Tiempo} from '../modelos/tiempo';
import{Trafico} from '../modelos/trafico';
import {Videos} from '../modelos/videos';
import {Twitter} from '../modelos/twitter';
import {TRAFICO, TRAFICO2} from '../controlador/controlconfig';
import {TIEMPO, TIEMPO2} from '../controlador/controlconfig';
import {VIDEO, VIDEO2} from '../controlador/controlconfig';
import {TWITS, TWITS2} from '../controlador/controlconfig';
import {TITULO, TITULO2} from '../controlador/controlconfig';
export interface Vid {
  lugar:string;
  visible:boolean;
  localoweb:boolean;
}
export interface RootObject {
  titulo:string;
  tiempo: Tiempo;
  trafico: Trafico;
  videos: Vid;
  twitter: Twitter;
}

@Injectable()
export class PreferenciaService {
  tiempos=TIEMPO;
  traficos=TRAFICO;
  videoss=VIDEO;
  twitters=TWITS;
  titulo=TITULO;
  tiempos2=TIEMPO2;
  traficos2=TRAFICO2;
  videoss2=VIDEO2;
  twitters2=TWITS2;
  titulo2=TITULO2;
  data;
  t;
  datos;
  constructor(private http:HttpClient, private _electronService: ElectronService,private zone:NgZone) { }
  preferencia(){
    this._electronService.ipcRenderer.send('leeme');
    this._electronService.ipcRenderer.addListener('leeme_reply',(evt,arg)=> {
        this.zone.runOutsideAngular(()=>{
          this.data=JSON.parse(arg);
          this.titulo.titulo=this.data.titulo;
          this.titulo2.titulo=this.data.titulo;

          this.videoss.lugar=this.data.videos.lugar;
          this.videoss.localoweb=this.data.videos.localoweb;
          this.videoss.visible=this.data.videos.visible;
          this.videoss2.lugar=this.data.videos.lugar;
          this.videoss2.localoweb=this.data.videos.localoweb;
          this.videoss2.visible=this.data.videos.visible;
          
 
          this.traficos.lugar=this.data.trafico.lugar;
          this.traficos.visible=this.data.trafico.visible;
          this.traficos.mapa=this.data.trafico.mapa;
          this.traficos.Lat=this.data.trafico.Lat;
          this.traficos.Lon=this.data.trafico.Lon;
          this.traficos2.lugar=this.data.trafico.lugar;
          this.traficos2.visible=this.data.trafico.visible;
          this.traficos2.mapa=this.data.trafico.mapa;
          this.traficos2.Lat=this.data.trafico.Lat;
          this.traficos2.Lon=this.data.trafico.Lon;

          this.tiempos.lugar=this.data.tiempo.lugar;
          this.tiempos.visible=this.data.tiempo.visible;
          this.tiempos.mapa=this.data.tiempo.mapa;
          this.tiempos.Lat=this.data.tiempo.Lat;
          this.tiempos.Lon=this.data.tiempo.Lon;
          this.tiempos.icono=this.data.tiempo.icono;
          
          this.tiempos2.lugar=this.data.tiempo.lugar;
          this.tiempos2.visible=this.data.tiempo.visible;
          this.tiempos2.mapa=this.data.tiempo.mapa;
          this.tiempos2.Lat=this.data.tiempo.Lat;
          this.tiempos2.Lon=this.data.tiempo.Lon;
          this.tiempos2.icono=this.data.tiempo.icono;
          console.log(this.tiempos2);
          this.twitters.abuscar=this.data.twitter.abuscar
          this.twitters.hashoprofile=this.data.twitter.hashoprofile;
          this.twitters.visible=this.data.twitter.visible;
          
          this.twitters2.abuscar=this.data.twitter.abuscar
          this.twitters2.hashoprofile=this.data.twitter.hashoprofile;
          this.twitters2.visible=this.data.twitter.visible;
        });
        this.zone.runTask(()=>{
          
        });
      });
  }
  guardapreferencia(datos){
    
    this._electronService.ipcRenderer.send('guardame',datos);
  }

}
