import { Injectable } from '@angular/core';
import { HttpClient , HttpHeaders} from '@angular/common/http';
import {Http, Headers} from '@angular/http';
import {TRAFICO} from '../controlador/controlconfig';
import {TIEMPO} from '../controlador/controlconfig';
export interface Address {
  town: string;
  county: string;
  state: string;
  postcode: string;
  country: string;
  country_code: string;
}

export interface RootObject {
  place_id: string;
  licence: string;
  osm_type: string;
  osm_id: string;
  boundingbox: string[];
  lat: string;
  lon: string;
  display_name: string;
  class: string;
  type: string;
  importance: number;
  icon: string;
  address: Address;
  svg: string;
}

@Injectable()
export class GeocodificarService {
  trafico=TRAFICO;
  tiempo=TIEMPO;
  datos:RootObject;
  constructor(private http:HttpClient) { }
  geocodif(){
        this.http.get<RootObject[]>('https://nominatim.openstreetmap.org/search/'+this.trafico.lugar+'?format=json&addressdetails=1&limit=1&polygon_svg=1').subscribe(data => {
          console.log(data[0].lat);
      
          this.trafico.Lat=parseFloat(data[0].lat);
          this.trafico.Lon=parseFloat(data[0].lon);
        });
    }
    geocodiftiempo(){
        this.http.get<RootObject[]>('https://nominatim.openstreetmap.org/search/'+this.tiempo.lugar+'?format=json&addressdetails=1&limit=1&polygon_svg=1').subscribe(data => {
          console.log(data[0].lat);
          this.tiempo.Lat=parseFloat(data[0].lat);
          this.tiempo.Lon=parseFloat(data[0].lon);
        });
    }

}
