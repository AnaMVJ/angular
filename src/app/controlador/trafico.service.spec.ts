import { TestBed, inject } from '@angular/core/testing';

import { TraficoService } from './trafico.service';

describe('TraficoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TraficoService]
    });
  });

  it('should be created', inject([TraficoService], (service: TraficoService) => {
    expect(service).toBeTruthy();
  }));
});
