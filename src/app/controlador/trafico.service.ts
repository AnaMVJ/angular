import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TRAFICO } from '../controlador/controlconfig';
import { Http, Headers } from '@angular/http';
export interface Point {
  type: string;
  coordinates: number[];
}

export interface ToPoint {
  type: string;
  coordinates: number[];
}

export interface Resource {
  __type: string;
  point: Point;
  description: string;
  end: Date;
  incidentId: any;
  lastModified: Date;
  roadClosed: boolean;
  severity: number;
  source: number;
  start: Date;
  toPoint: ToPoint;
  type: number;
  verified: boolean;
}

export interface ResourceSet {
  estimatedTotal: number;
  resources: Resource[];
}

export interface RootObject {
  authenticationResultCode: string;
  brandLogoUri: string;
  copyright: string;
  resourceSets: ResourceSet[];
  statusCode: number;
  statusDescription: string;
  traceId: string;
}



@Injectable()
export class TraficoService {
  traf = TRAFICO;
  datos: Resource[];
  constructor(private http: HttpClient) { }
  trafico() {
    var latitude = this.traf.Lat - 0.5;
    var latitude2 = this.traf.Lat + 0.5;
    var longitude = this.traf.Lon - 0.5;
    var longitude2 = this.traf.Lon + 0.5;
    return this.http.get<RootObject>('http://dev.virtualearth.net/REST/v1/Traffic/Incidents/' + latitude + ',' + longitude + ',' + latitude2 + ',' + longitude2 + '?key=ApY2CWzJofbaGRsMsWLEN9puw5QLFH9RQ1y72F83STiLnN9Hh0ma6ldgTcz9x8t9');

  }


}
