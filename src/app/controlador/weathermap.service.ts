import { Injectable } from '@angular/core';
import { HttpClient , HttpHeaders} from '@angular/common/http';
import {Http, Headers} from '@angular/http';
export interface Coord {
  Lat: number;
  Lon: number;
}

export interface Main {
  temp: number;
  temp_min: number;
  temp_max: number;
  pressure: number;
  humidity: number;
}

export interface Wind {
  speed: number;
  deg: number;
}

export interface Clouds {
  today: number;
}

export interface Weather {
  id: number;
  main: string;
  description: string;
  icon: string;
}

export interface List {
  id: number;
  dt: number;
  name: string;
  coord: Coord;
  main: Main;
  wind: Wind;
  rain?: any;
  snow?: any;
  clouds: Clouds;
  weather: Weather[];
}

export interface RootObject {
  cod: number;
  calctime: number;
  cnt: number;
  list: List[];
}
@Injectable()

export class WeathermapService {

  constructor(private http:HttpClient) { }
  tiempo(westLng,northLat,eastLng,southLat){
          
    return this.http.get<RootObject>('http://api.openweathermap.org/data/2.5/box/city?bbox='
    + westLng + "," + northLat + "," //left top
    + eastLng + "," + southLat + "," //right bottom
    + '10&cluster=yes&format=json'+'&appid=12217a05e7ae0e4f83ef27620ef75943&lang=es');   
}
}
