import { Injectable,NgZone } from '@angular/core';
import { HttpClient , HttpHeaders} from '@angular/common/http';
import {Http, Headers} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { catchError, map, tap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { RootData } from '@angular/core/src/view';
import { TWITS} from './controlconfig';
import {NgxElectronModule} from 'ngx-electron';
import {ElectronService} from 'ngx-electron';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/X-www-form-urlencoded' })
};
export interface Hashtag {
  text: string;
  indices: number[];
}



export interface User {
  id: any;
  id_str: string;
  name: string;
  screen_name: string;
  location: string;
  description: string;
  url: string;
  entities;
  protected: boolean;
  followers_count: number;
  friends_count: number;
  listed_count: number;
  created_at: string;
  favourites_count: number;
  utc_offset?: number;
  time_zone: string;
  geo_enabled: boolean;
  verified: boolean;
  statuses_count: number;
  lang: string;
  contributors_enabled: boolean;
  is_translator: boolean;
  is_translation_enabled: boolean;
  profile_background_color: string;
  profile_background_image_url: string;
  profile_background_image_url_https: string;
  profile_background_tile: boolean;
  profile_image_url: string;
  profile_image_url_https: string;
  profile_banner_url: string;
  profile_link_color: string;
  profile_sidebar_border_color: string;
  profile_sidebar_fill_color: string;
  profile_text_color: string;
  profile_use_background_image: boolean;
  has_extended_profile: boolean;
  default_profile: boolean;
  default_profile_image: boolean;
  following?: any;
  follow_request_sent?: any;
  notifications?: any;
  translator_type: string;
}



export interface Place {
  id: string;
  url: string;
  place_type: string;
  name: string;
  full_name: string;
  country_code: string;
  country: string;
  contained_within: any[];
  bounding_box;
  attributes;
}









export interface Status {
  created_at: string;
  id: any;
  id_str: string;
  text: string;
  truncated: boolean;
  entities;
  metadata;
  source: string;
  in_reply_to_status_id?: number;
  in_reply_to_status_id_str: string;
  in_reply_to_user_id?: number;
  in_reply_to_user_id_str: string;
  in_reply_to_screen_name: string;
  user: User;
  geo?: any;
  coordinates?: any;
  place?: any;
  contributors?: any;
  retweeted_status;
  is_quote_status: boolean;
  quoted_status_id: number;
  quoted_status_id_str: string;
  retweet_count: number;
  favorite_count: number;
  favorited: boolean;
  retweeted: boolean;
  lang: string;
  extended_entities;
  possibly_sensitive?: boolean;
}



export interface Data {
  statuses: Status[];
  search_metadata
}

export interface RootObject {
  data: Data;
}
@Injectable()
export class MistwitsService {
  searchquery = '';
  public data: Data;
  public tweetsdata: Array<Status> = [];
  twits=TWITS;
  
  constructor(private http:HttpClient,private _electronService: ElectronService,private zone:NgZone) { 
    
    
  }
  call(){
    var searchterm = this.twits.abuscar;
    this._electronService.ipcRenderer.addListener('usuario_reply',(evt,arg) =>{console.log(arg)});
    if(!this.twits.hashoprofile){
      this._electronService.ipcRenderer.send('perfil',searchterm);
      this._electronService.ipcRenderer.addListener('perfil_reply',(evt,arg)=> {
          this.zone.runOutsideAngular(()=>{
            this.tweetsdata=JSON.parse(arg);
          });
          this.zone.runTask(()=>{//console.log(this.tweetsdata);
          });
          

      });
          
      
    }else{
      this._electronService.ipcRenderer.send('buscame',searchterm);
      this._electronService.ipcRenderer.addListener('buscame_reply',(evt,arg)=> {
        this.zone.runOutsideAngular(()=>{  
          this.data=JSON.parse(arg);
          this.tweetsdata=this.data.statuses;
        });
        this.zone.runTask(()=>{ //console.log(this.tweetsdata);
        });
          
      });
      
    }
  }
  
  addValue(value){
    
    this.tweetsdata.push(value);
    
  }

}
