import {Videos} from '../modelos/videos';
import {Twitter} from '../modelos/twitter';
import {Tiempo} from '../modelos/tiempo';
import {Trafico} from '../modelos/trafico';
import {Carga} from '../modelos/carga';
import { } from '@types/googlemaps';
import { DomSanitizer } from '@angular/platform-browser/src/security/dom_sanitization_service';
export const TITULO={titulo:"Panel"};
export const VIDEO:Videos={lugar:"https://www.w3schools.com/tags/movie.ogg",visible:false,file:null,localoweb:false};
export const TWITS:Twitter={abuscar:"Yasmin_Payne1",visible:false,hashoprofile:false};   
export const TIEMPO:Tiempo={visible:false,lugar:'Zurich',Lon:8.541694000000007,Lat:47.3768866,icono:'',mapa:false};
export const TRAFICO:Trafico={visible:false,lugar:'Zurich',Lon:8.541694000000007,Lat:47.3768866,mapa:false};
export const CARGA:Carga={load:true};
export const TITULO2={titulo:"Panel"};
export const VIDEO2:Videos={lugar:"https://www.w3schools.com/tags/movie.ogg",visible:false,file:null,localoweb:false};
export const TWITS2:Twitter={abuscar:"Yasmin_Payne1",visible:false,hashoprofile:false};   
export const TIEMPO2:Tiempo={visible:false,lugar:'Zurich',Lon:8.541694000000007,Lat:47.3768866,icono:'',mapa:false};
export const TRAFICO2:Trafico={visible:false,lugar:'Zurich',Lon:8.541694000000007,Lat:47.3768866,mapa:false};

export class ControlConfig{
    video=VIDEO;
    twits=TWITS;
    trafico=TRAFICO;
    titulo=TITULO;
    tiempo=TIEMPO;
    constructor( ){}  
    
    onFileChange(file) {    
         this.video.file=file;
    }
    copiatwits(twittes:Twitter){
        this.twits.hashoprofile=twittes.hashoprofile;
        this.twits.abuscar=twittes.abuscar;
        this.twits.visible=twittes.visible;
    }
    copiatraf( traf:Trafico) 
    {     
        this.trafico.visible=traf.visible;
        this.trafico.lugar=traf.lugar;
        this.trafico.mapa=traf.mapa;
        this.trafico.Lat=traf.Lat;
        this.trafico.Lon=traf.Lon;
    }
    copiatiempo( tiemp:Tiempo):void 
    {     
        this.tiempo.visible=tiemp.visible;
        this.tiempo.lugar=tiemp.lugar;
        this.tiempo.icono=tiemp.icono;
        this.tiempo.Lon=tiemp.Lon;
        this.tiempo.Lat=tiemp.Lat;
        this.tiempo.mapa=tiemp.mapa;
    }
    copiavideos (videos:Videos) {
        this.video.lugar=videos.lugar;
        this.video.visible=videos.visible;
        this.video.localoweb=videos.localoweb;
        //this.video.file=videos.file;
    }


} 
