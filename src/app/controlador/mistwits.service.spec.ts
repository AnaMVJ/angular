import { TestBed, inject } from '@angular/core/testing';

import { MistwitsService } from './mistwits.service';

describe('MistwitsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MistwitsService]
    });
  });

  it('should be created', inject([MistwitsService], (service: MistwitsService) => {
    expect(service).toBeTruthy();
  }));
});
