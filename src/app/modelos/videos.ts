import { SafeUrl } from "@angular/platform-browser/src/security/dom_sanitization_service";

export class Videos{
    lugar:string;
    visible:boolean;
    localoweb:boolean;
    file:File;
    
    constructor( video:Videos) {
        this.lugar=video.lugar;
        this.visible=video.visible;
        this.localoweb=video.localoweb;
        this.file=video.file;
    }
   
    
}