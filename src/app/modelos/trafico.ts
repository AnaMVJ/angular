export class Trafico{
    visible:boolean;
    lugar:String;
    mapa:boolean;
    Lon:number;
    Lat:number;
    constructor( traf:Trafico) 
    {     
        this.visible=traf.visible;
        this.lugar=traf.lugar;
        this.mapa=traf.mapa;
        this.Lat=traf.Lat;
        this.Lon=traf.Lon;
    }
    
}