import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConfigurationComponent }      from './vistas/configuration/configuration.component';
import { VistaComponent }      from './vistas/vista/vista.component';
import { AboutComponent }      from './vistas/about/about.component';
const routes: Routes = [
  { path: 'configuration', component: ConfigurationComponent },
  { path: 'vista', component:VistaComponent},
  { path: 'about', component:AboutComponent}, 
  { path: '', redirectTo: '/configuration', pathMatch: 'full' }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
