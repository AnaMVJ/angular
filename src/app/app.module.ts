import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'; // <-- NgModel lives here

import { AppComponent } from './app.component';
import { ConfigurationComponent } from './vistas/configuration/configuration.component';
import { VistaComponent } from './vistas/vista/vista.component';
import { AppRoutingModule } from './/app-routing.module';
import { MistwitsService } from './controlador/mistwits.service';
import { HttpClientModule } from '@angular/common/http';
import { VideoComponent } from './vistas/video/video.component';
import { TwitterComponent } from './vistas/twitter/twitter.component';
import { TiempoService } from './controlador/tiempo.service';
import { TraficoService } from './controlador/trafico.service';
import { TiempoComponent } from './vistas/tiempo/tiempo.component';
import { TraficoComponent } from './vistas/trafico/trafico.component';
import { TraficotextoComponent } from './vistas/traficotexto/traficotexto.component';
import { MessageServiceService } from './controlador/message-service.service';
import { GeocodificarService } from './controlador/geocodificar.service';
import { TiempomapaComponent } from './vistas/tiempomapa/tiempomapa.component';
import { WeathermapService } from './controlador/weathermap.service';

import {NgxElectronModule} from 'ngx-electron';
import { PreferenciaService } from './controlador/preferencia.service';
import { AboutComponent } from './vistas/about/about.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MatToolbarModule,  
  MatTabsModule,
  MatButtonModule,
  MatIconModule,
  MatCardModule,
  MatCheckboxModule,
  MatTooltipModule,
  MatInputModule,
  MatGridListModule,
  MatFormFieldModule
} from '@angular/material';
@NgModule({
  declarations: [
    AppComponent,
    ConfigurationComponent,
    VistaComponent,
    VideoComponent,
    TwitterComponent,
    TiempoComponent,
    TraficoComponent,
    TraficotextoComponent,
    TiempomapaComponent,
    AboutComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgxElectronModule,
    MatToolbarModule,
    MatTabsModule,
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatCheckboxModule,
    MatTooltipModule,
    MatInputModule,
    MatFormFieldModule,
    MatGridListModule,
    BrowserAnimationsModule
  ],
  providers: [MistwitsService, TiempoService, TraficoService, MessageServiceService, GeocodificarService, WeathermapService, PreferenciaService],
  bootstrap: [AppComponent]
})
export class AppModule { }
